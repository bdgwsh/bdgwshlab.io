---
title: "Итераторы в Python"
date: 2019-03-01T20:22:18+03:00
linktitle: "Итераторы в python"
tags: [
  "python",
  "итераторы"
]
categories: [
    "python",
    "разработка",
]
---

## **Понятие итератора и итерируемых объектов**
Если говорить вне контекста *python*, то итератор это шаблон проектирования, который описывает объект, позволяющий проходиться по элементам некоторой коллекции.
Если обратиться к книге банды четырех, то описание этого паттерна звучит так:

> Итератор предоставляет способ последовательного доступа ко всем элементам составного объекта, не раскрывая его внутреннего представления.
> Основная его идея состоит в том, чтобы за доступ к элементам и способ обхода отвечал не сам список, а отдельный объект итератор.

Разделение итератора от основного класса коллекции позволяет определять различные алгоритмы обхода, не изменяя сам класс коллекции. В классе коллекции должены быть определен метод, возвращающий объект итератор.

### Применимость
* для доступа к содержимому объектов коллекции без раскрытия внутренного представления коллекции
* для поддержки разного рода обходов по коллекции
* для для предоставления интерфейса обхода различных объектов


## Итераторы и итерируемые объекты в **python**

В *python* с данным паттерном вы можете столкнуться, например, при итерировании по списку:
```python
names = ['Elon', 'Guido', 'Bjern']
for name in names:
    print(name)
```

На самом деле при итерировании по списку сперва получается итератор списка.
```
>>> names_iter = iter(names)
>>> type(names_iter)
<class 'list_iterator'>
```
Класс `list_iterator` и есть специальный итератор, с помощью которого осуществляется итерирование. Вызовем функцию `next()`, передав объект-итератор в качестве аргумента.
```
>>> next(names_iter)
'Elon'
>>> next(names_iter)
'Guido'
```
Функция `next` возвращает следующий элемент итерируемого объекта, но когда мы полностью проитерируемся по объекту, то выбросится исключение `StopIteration`.
```
>>> next(names_iter)
'Bjern'
>>> next(names_iter)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
```
Таким образом, все итерируемые объекты выполняют вышеописанные операции.

## Собственный итератор
Допустим, мы хотим создать объект, при итерации по которому будет генерироваться *uuid*.
Для этого необходимо описать класс, реализующий методы *`__iter__`* и возвращающий итератор. Сам итератор должен быть представлен в виде ещё одного класса,
реализующего методы *`__next__`* и *`__iter__`*. Реализуем для начала итератор.
```
import uuid

class UUIDIterator:
    """
    Итератор для генерации uuid идентификаторов.
    """

    def __init__(self, limit):
        self._limit = limit
        self._index = 0

    def __next__(self):
        if self._index < self._limit:
            self._index += 1
            return uuid.uuid1()
        raise StopIteration
```
В представленном классе реализован метод *`__next__`*, который возвращает *uuid* до тех пор, пока не достигнет лимита, а затем генерирует исключение `StopIteration`.
Теперь если мы вызовем функцию *`iter`* на экземпляре класса этого итератора, то получим то, что возвращает метод *`__next__`*.
```
>>> next(UUIDIterator(2))
UUID('8a091986-3c5f-11e9-a1bd-701ce791b04a')
```
Однако по данному объекту нельзя проитерироваться.
```
>>> for i in UUIDIterator(2):
...     print(i)
...
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'UUIDIterator' object is not iterable
```
Для этого необходимо сделать его итерируемым, реализовав метод *`__iter__`*, который вернет экземпляр класса итератора.

```
class UUIDIterator:
    """
    Итератор для генерации uuid идентификаторов.
    """

    def __init__(self, limit):
        self._limit = limit
        self._index = 0

    def __next__(self):
        if self._index < self._limit:
            self._index += 1
            return uuid.uuid1()
        raise StopIteration

    def __iter__(self):
        """
        Метод, возвращающий объект-итератор.
        :return: объект-итератор (UUIDIterator)
        """
        return self
```

Так наш итератор станет итерируемым объектом.
```
>>> for i in UUIDIterator(2):
...     print(i)
...
46373534-3c60-11e9-93a2-701ce791b04a
4637407e-3c60-11e9-93a2-701ce791b04a
```
Такой подход, однако, не совсем корректный. Понятия итератора и итерируемого объекта это разные понятия. Правильно выделить итерируемый объект и итератор в отдельные классы.

```
class UUIDIterator:
    """
    Итератор для генерации uuid идентификаторов.
    """

    def __init__(self, limit):
        self._limit = limit
        self._index = 0

    def __next__(self):
        if self._index < self._limit:
            self._index += 1
            return uuid.uuid1()
        raise StopIteration


class UUIDRange:
    """
    Итерируемый объект, который использует итератор UUIDIterator.
    """

    def __init__(self, count=1):
        self.count = count

    def __iter__(self):
        """
        Метод, который возвращает объект-итератор.
        :return: объект-итератор (UUIDIterator)
        """
        return UUIDIterator(self.count)
```
Теперь проитерируемся по итерируемому объекту.
```
>>> for uuid_ in UUIDRange(3):
>>>     print(uuid_)
3f7cbd22-3c60-11e9-93a2-701ce791b04a
3f7cc024-3c60-11e9-93a2-701ce791b04a
3f7cc196-3c60-11e9-93a2-701ce791b04a
```

Таким образом, можно сказать, что паттерн итератор встроен в python и позволяет очень удобно проходиться по элементам коллекции. Понятие итератора и итериуемого объекта следует отличать друг от друга. Итерируемый объект, как правило, это объект, по которому можно проитерироваться в цикле `for item in iterable`, а итератор это объект, который реализует логику итерации. Данная концепция достаточно простая, но довольно важная. Помимо концепции итератора в python имеются генераторы, которые выполняют схожие фукнции. Поговорим о генераторах в следующей статье.
